package com.unabtesting.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.app.usage.UsageEvents;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button button0, button1, button2, button3, button4, button5, button6,
            button7, button8, button9, buttonAdd, buttonSub, buttonDivision,
            buttonMul, button10, buttonC, buttonEqual;
    ArrayList<ButtonData> m_buttonsData = new ArrayList<>();

    EditText textOutputCell;

    float mValueOne, mValueTwo;

    boolean inAddition, mSubtract, inMultiplication, inDivision;

    class ButtonData{
        public Button m_button;
        public int m_buttonId;
        public String m_actionOnClick;
        public String m_label;
        public MainActivity m_mainActivity;
        public ButtonData m_this;

        public ButtonData(int buttonId, String label, MainActivity mainActivity){
            m_buttonId = buttonId;
            //m_actionOnClick = actionOnClick;
            m_label = label;
            m_mainActivity = mainActivity;
            m_button = (Button) findViewById(buttonId);
            m_this = this;

            m_button.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            m_this.ButtonOnClick();
                        }
                    }
            );
        }

        public void ButtonOnClick(){
            m_mainActivity.OnButtonClick(m_buttonId);
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_buttonsData.add(new ButtonData(R.id.button0,"0",this));
        m_buttonsData.add(new ButtonData(R.id.button1,"1",this));
        m_buttonsData.add(new ButtonData(R.id.button2,"2",this));
        m_buttonsData.add(new ButtonData(R.id.button3,"3",this));
        m_buttonsData.add(new ButtonData(R.id.button4,"4",this));
        m_buttonsData.add(new ButtonData(R.id.button5,"5",this));
        m_buttonsData.add(new ButtonData(R.id.button6,"6",this));
        m_buttonsData.add(new ButtonData(R.id.button7,"7",this));
        m_buttonsData.add(new ButtonData(R.id.button8,"8",this));
        m_buttonsData.add(new ButtonData(R.id.button9,"9",this));

        button10 = (Button) findViewById(R.id.button10);
        buttonAdd = (Button) findViewById(R.id.buttonadd);
        buttonSub = (Button) findViewById(R.id.buttonsub);
        buttonMul = (Button) findViewById(R.id.buttonmul);
        buttonDivision = (Button) findViewById(R.id.buttondiv);
        buttonC = (Button) findViewById(R.id.buttonC);
        buttonEqual = (Button) findViewById(R.id.buttoneql);
        textOutputCell = (EditText) findViewById(R.id.edt1);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (textOutputCell == null) {
                    textOutputCell.setText("");
                } else {
                    mValueOne = Float.parseFloat(textOutputCell.getText() + "");
                    inAddition = true;
                    textOutputCell.setText(null);
                }
            }
        });

        buttonSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Float.parseFloat(textOutputCell.getText() + "");
                mSubtract = true;
                textOutputCell.setText(null);
            }
        });

        buttonMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Float.parseFloat(textOutputCell.getText() + "");
                inMultiplication = true;
                textOutputCell.setText(null);
            }
        });

        buttonDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Float.parseFloat(textOutputCell.getText() + "");
                inDivision = true;
                textOutputCell.setText(null);
            }
        });

        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueTwo = Float.parseFloat(textOutputCell.getText() + "");

                if (inAddition == true) {
                    textOutputCell.setText(mValueOne + mValueTwo + "");
                    inAddition = false;
                }

                if (mSubtract == true) {
                    textOutputCell.setText(mValueOne - mValueTwo + "");
                    mSubtract = false;
                }

                if (inMultiplication == true) {
                    textOutputCell.setText(mValueOne * mValueTwo + "");
                    inMultiplication = false;
                }

                if (inDivision == true) {
                    textOutputCell.setText(mValueOne / mValueTwo + "");
                    inDivision = false;
                }
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textOutputCell.setText("");
            }
        });

        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textOutputCell.setText(textOutputCell.getText() + ".");
            }
        });
    }

    public void OnButtonClick(int buttonIndex){

        switch (buttonIndex){
            case R.id.button0:
                AddDigitOnOutputCell("0");
                break;
            case R.id.button1:
                AddDigitOnOutputCell("1");
                break;
            case R.id.button2:
                AddDigitOnOutputCell("2");
                break;
            case R.id.button3:
                AddDigitOnOutputCell("3");
                break;
            case R.id.button4:
                AddDigitOnOutputCell("4");
                break;
            case R.id.button5:
                AddDigitOnOutputCell("5");
                break;
            case R.id.button6:
                AddDigitOnOutputCell("6");
                break;
            case R.id.button7:
                AddDigitOnOutputCell("7");
                break;
            case R.id.button8:
                AddDigitOnOutputCell("8");
                break;
            case R.id.button9:
                AddDigitOnOutputCell("9");
                break;
        }
    }

    public String ConcatStringDigit(String currOutputCell, String digitToAdd){
        return currOutputCell + digitToAdd;
    }

    public void AddDigitOnOutputCell(String digit){
        String digitOnOutputCell = textOutputCell.getText().toString();
        String finalDigit = ConcatStringDigit(digitOnOutputCell,digit);
        textOutputCell.setText(finalDigit);
    }
}
